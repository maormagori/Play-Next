package com.productions.mx2.playnext;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VideosDatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Videos.db";
    public static final String TABLE_NAME = "videos_table";
    public static final String ID = "ID";
    public static final String TITLE = "TITLE";
    public static final String DURATION = "DURATION";
    public static final String UPLOADER = "UPLOADER";
    public static final String THUMBNAIL = "THUMBNAIL";


    public VideosDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME + " (ID TEXT PRIMARY KEY,TITLE TEXT,DURATION TEXT,UPLOADER TEXT, THUMBNAIL BLOB)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public int insertVideo(String id, String title, String duration, String uploader, byte[] image) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (isExists(id, db))
            return 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, id);
        contentValues.put(TITLE, title);
        contentValues.put(DURATION, duration);
        contentValues.put(UPLOADER, uploader);
        contentValues.put(THUMBNAIL, image);

        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1)
            return -1;
        return 1;
    }

    private boolean isExists(String id, SQLiteDatabase db) {
        String Query = "Select * from " + TABLE_NAME + " where " + ID + " = " + "?";
        Cursor cursor = db.rawQuery(Query, new String[]{id});
        if(cursor.getCount()<=0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }

    public Integer deleteData (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?",new String[] {id});
    }

    public List<HashMap> getAllDataList(){
        Cursor cursor=getAllData();
        List<HashMap> videos=new ArrayList<>();
        while (cursor.moveToNext()){
            HashMap video=new HashMap();
            video.put(ID, cursor.getString(0));
            video.put(TITLE, cursor.getString(1));
            video.put(DURATION, cursor.getString(2));
            video.put(UPLOADER, cursor.getString(3));
            video.put(THUMBNAIL,byteArrayToBitmap(cursor));
            videos.add(video);
        }
        cursor.close();
        return videos;
    }

    private Bitmap byteArrayToBitmap(Cursor cursor){
        byte[] imgByte=cursor.getBlob(4);
        return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
    }
}
