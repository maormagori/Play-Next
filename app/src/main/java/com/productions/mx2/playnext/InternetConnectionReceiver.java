package com.productions.mx2.playnext;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class InternetConnectionReceiver extends BroadcastReceiver {
    Snackbar connectionSnackbar;

    public InternetConnectionReceiver() {
        super();

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

            if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                if (connectionSnackbar != null && connectionSnackbar.isShown())
                    connectionSnackbar.dismiss();
            }
            else
                if(connectionSnackbar != null && !connectionSnackbar.isShown())
                    connectionSnackbar.show();
                else {
                    connectionSnackbar = Snackbar.make(((AppCompatActivity) context).getSupportFragmentManager().findFragmentById(R.id.video_list_container).getView()
                            , R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE);
                    ((TextView) connectionSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor(Color.RED);
                    connectionSnackbar.show();
                }
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }


    }
}
