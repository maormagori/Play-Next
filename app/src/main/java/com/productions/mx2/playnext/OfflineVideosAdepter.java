package com.productions.mx2.playnext;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

class OfflineVideosAdepter extends RecyclerView.Adapter<VideoViewHolder> {
    List<HashMap> videos;
    Context context;
    DatabaseReference roomReference;
    final float titleSize = 14f;
    final float infoSize = 12f;
    String nickname;

    public OfflineVideosAdepter(Context context, List<HashMap> videos, DatabaseReference roomReference, String nickname) {
        this.context = context;
        this.videos=videos;
        this.roomReference = roomReference;
        this.nickname = nickname;

    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_video, parent, false);
        return new VideoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoViewHolder holder,final int position) {
        final HashMap video = videos.get(position);
        final View.OnClickListener holderClicked=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new VideoUploader().uploadVideo(roomReference, (String)video.get(VideosDatabaseHelper.ID), nickname)
                            .addOnSuccessListener(new OnSuccessListener<Integer>() {
                                @Override
                                public void onSuccess(Integer integer) {
                                    switch (integer) {
                                        case VideoUploader.UPLOAD_SUCCESSFULLY:
                                            Toast.makeText(context, R.string.video_uploaded_successfully, Toast.LENGTH_SHORT).show();
                                            break;
                                        case VideoUploader.UPLOAD_FAILED:
                                            Toast.makeText(context, R.string.video_upload_failed, Toast.LENGTH_SHORT).show();
                                            break;
                                        case VideoUploader.VIDEO_EXISTS:
                                            Toast.makeText(context, R.string.video_already_exists, Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, R.string.video_upload_failed, Toast.LENGTH_SHORT).show();
                }
            }
        };
        holder.title.setTextSize(TypedValue.COMPLEX_UNIT_SP,titleSize);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.title.getLayoutParams();
        params.setMargins(0,0,0,0);
        holder.title.setLayoutParams(params);
        holder.length.setTextSize(TypedValue.COMPLEX_UNIT_SP,infoSize);
        holder.user.setTextSize(TypedValue.COMPLEX_UNIT_SP,infoSize);
        holder.getView().setTag((String)video.get(VideosDatabaseHelper.ID));
        holder.thumbnail.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                try {
                    holder.title.setText((String) video.get(VideosDatabaseHelper.TITLE));
                    holder.length.setText((String) video.get(VideosDatabaseHelper.DURATION));
                    holder.user.setText((String) video.get(VideosDatabaseHelper.UPLOADER));
                    holder.getView().setOnClickListener(holderClicked);
                    return true;
                }
                finally {
                    holder.thumbnail.getViewTreeObserver().removeOnPreDrawListener(this);
                }
            }
        });
        holder.thumbnail.setImageBitmap((Bitmap) video.get(VideosDatabaseHelper.THUMBNAIL));

    }


    @Override
    public int getItemCount() {
        return videos.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void updateData(List<HashMap> allDataList) {
        videos = allDataList;
        notifyDataSetChanged();
    }
}
