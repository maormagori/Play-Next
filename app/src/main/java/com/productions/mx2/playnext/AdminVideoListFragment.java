package com.productions.mx2.playnext;


import android.databinding.ObservableArrayList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class AdminVideoListFragment extends Fragment {


    String roomname;
    String nickname;
    DatabaseReference databaseReference;
    RecyclerView videoRecyclerView;
    LinearLayoutManager linearLayoutManager;
    static ObservableArrayList<Video> allVideos;
    VideoListAdepter videoListAdepter;
    Video currentVideo;
    ChildEventListener videoAddedEventListner;
    AddVideosFAB fab;

    public AdminVideoListFragment() {


    }

    public static AdminVideoListFragment newInstance() {
        AdminVideoListFragment fragment = new AdminVideoListFragment();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        fab = getView().findViewById(R.id.fab);
        videoRecyclerView= (RecyclerView) getView().findViewById(R.id.video_recycler_view);
        linearLayoutManager=new LinearLayoutManager(getContext());
        videoRecyclerView.setLayoutManager(linearLayoutManager);
        videoListAdepter = new VideoListAdepter(getContext(), allVideos);
        videoRecyclerView.setAdapter(videoListAdepter);


        fab.initialize(databaseReference, nickname);


        if(databaseReference!=null) {
            databaseReference.addChildEventListener(videoAddedEventListner);
        }

    }

    //TODO: Fix what's causing the if statement.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(getArguments()!=null) {
            Log.i("AdminVideoListFragment", "Got Arguments");
            roomname= getArguments().getString("roomname");
            nickname = getArguments().getString("nickname");
            databaseReference= FirebaseDatabase.getInstance().getReference().child(roomname);
        }

        videoAddedEventListner=new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("currentVideo"))
                    setCurrentVideo();
                else
                    if(!dataSnapshot.getKey().equals("initialized"))
                        loadVideos(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        allVideos=new ObservableArrayList<Video>();
        View layout = inflater.inflate(R.layout.fragment_admin_video_list, container, false);
        return layout;

    }

    public void setCurrentVideo() {
        databaseReference.child("currentVideo").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    currentVideo = dataSnapshot.getValue(Video.class);
                    currentVideoPlayedBG();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
    }



    public Video getCurrentVideo(){
        return this.currentVideo;
    }

    public void clearVideoBG(Video video) {
        int index = allVideos.indexOf(video);
        linearLayoutManager.findViewByPosition(index).setBackgroundColor(getResources().getColor(R.color.videoBeingPlayedBG));
        linearLayoutManager.findViewByPosition(index).setBackgroundResource(0);
    }

    private void loadVideos(DataSnapshot dataSnapshot) {
        Video video=dataSnapshot.getValue(Video.class);
        allVideos.add(video);
        videoListAdepter.notifyItemInserted(allVideos.size()-1);

    }

    public void currentVideoPlayedBG(){
        int index = allVideos.indexOf(currentVideo);
        if(videoListAdepter == null || linearLayoutManager.findViewByPosition(index) ==null)
            return;
        linearLayoutManager.findViewByPosition(index).setBackgroundColor(getResources().getColor(R.color.videoBeingPlayedBG));
        if (index!=0)
            clearVideoBG(allVideos.get(index-1));
    }

    public Snackbar makeSnackbar(int text, int duration, int textColor){
        Snackbar snackbar=makeSnackbar(text,duration);
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor(textColor);
        return snackbar;
    }

    private Snackbar makeSnackbar(int text, int duration) {
        return Snackbar.make(getView(), text,duration);
    }

}
