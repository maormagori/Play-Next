package com.productions.mx2.playnext;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DatabaseReference;

import org.jetbrains.annotations.NotNull;

import uk.co.markormesher.android_fab.FloatingActionButton;
import uk.co.markormesher.android_fab.SpeedDialMenuAdapter;
import uk.co.markormesher.android_fab.SpeedDialMenuItem;

public class AddVideosFAB extends FloatingActionButton {
    Context context;
    String nickname;
    DatabaseReference roomReference;
    UploadVideosSpeedDialAdepter speedDialAdepter;

    public AddVideosFAB(@NotNull Context context) {
        super(context);
        this.context=context;
    }

    public AddVideosFAB(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);
        this.context=context;

    }

    public AddVideosFAB(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
    }

    public void initialize(DatabaseReference databaseReference, String name){
        roomReference =databaseReference;
        nickname=name;
        bringToFront();
        setContentCoverColour(0xccffffff);
        setContentCoverEnabled(true);
        speedDialAdepter=new UploadVideosSpeedDialAdepter();
        setSpeedDialMenuAdapter(speedDialAdepter);

    }

    private class UploadVideosSpeedDialAdepter extends SpeedDialMenuAdapter{

        DialogInterface.OnDismissListener closeSpeedDialAfterDialog;

        public UploadVideosSpeedDialAdepter() {
            super();
            closeSpeedDialAfterDialog =new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if(isSpeedDialMenuOpen())
                        closeSpeedDialMenu();
                }
            };
        }

        @Override
        public int getCount() {
            return 2;
        }

        @NotNull
        @Override
        public SpeedDialMenuItem getMenuItem(@NotNull Context context, int position) {
            switch (position){
                case 0: SpeedDialMenuItem search= new SpeedDialMenuItem(context,R.drawable.ic_search_white_24dp, R.string.search_videos);
                    return search;
                case 1: SpeedDialMenuItem offlineVideos= new SpeedDialMenuItem(context,R.drawable.ic_file_upload_white_24dp, R.string.offline_videos);
                    return offlineVideos;
                default: break;
            }
            return new SpeedDialMenuItem(context, R.drawable.ic_file_upload_white_24dp,Integer.toString(position));
        }


        @Override
        public boolean onMenuItemClick(int position) {
            switch (position){
                case 0: MaterialDialog searchDialog =DialogHelper.addVideoDialog(context, roomReference, nickname);
                        searchDialog.setOnDismissListener(closeSpeedDialAfterDialog);
                        break;
                case 1:MaterialDialog offlineVideoDialog=DialogHelper.addOfflineVideoDialog(context, roomReference, nickname);
                    offlineVideoDialog.setOnDismissListener(closeSpeedDialAfterDialog);
                    break;

            }
            return false;
        }

        @Override
        public float fabRotationDegrees() {
            return 135F;
        }
    }
}
