package com.productions.mx2.playnext;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.ObservableList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import uk.co.markormesher.android_fab.FloatingActionButton;
import uk.co.markormesher.android_fab.SpeedDialMenuOpenListener;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RoomAdminActivity extends AppCompatActivity {
    boolean initialized;
    YouTubePlayer.PlayerStateChangeListener playerStateChangeListener;
    YouTubePlayer TubePlayer;
    YouTubePlayerFragment youTubePlayerFragment;
    String nickname;
    String roomname;
    DatabaseReference databaseReference;
    StorageReference iconReference;
    Video currentVideo;
    InternetConnectionReceiver connectionReceiver;
    ValueEventListener roomClosedListener;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_admin);

        currentVideo=new Video();

        //TODO: Set margin between logo and back arrow to 0.
        Toolbar toolbar =  findViewById(R.id.admin_toolbar);
        setSupportActionBar(toolbar);
        initialized = getIntent().getBooleanExtra("initialized", false);
        nickname=getIntent().getExtras().getString("nickname");
        roomname=getIntent().getExtras().getString("roomname");
        getSupportActionBar().setTitle(roomname);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        iconReference= FirebaseStorage.getInstance().getReference().child("rooms/"+roomname+".jpg");
        iconReference.getBytes(1024*1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                RoundedBitmapDrawable roundedBitmapDrawable= RoundedBitmapDrawableFactory.create(getResources(),BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
                roundedBitmapDrawable.setCircular(true);
                getSupportActionBar().setLogo(roundedBitmapDrawable);
                Toast.makeText(getBaseContext(),getResources().getString(R.string.successfully_loaded)+ " " +roomname+ " " +getResources().getString(R.string.logo),Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getBaseContext(),getResources().getString(R.string.unsuccessfully_loaded)+ " " +roomname+ " " +getResources().getString(R.string.logo),Toast.LENGTH_SHORT).show();
            }
        });

        databaseReference= FirebaseDatabase.getInstance().getReference().child(roomname);

        AdminVideoListFragment adminVideoListFragment=new AdminVideoListFragment();
        adminVideoListFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().replace(R.id.video_list_container, adminVideoListFragment).commit();



        playerStateChangeListener= new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {
                nextVideo();

            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {
                Log.i("YouTube Player", "Error: " + errorReason.toString());
            }
        };

        youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtubePlayerFragment);
        if(initialized) {
            databaseReference.child("initialized").setValue(initialized);
            if (!getPreferences(MODE_PRIVATE).getBoolean("videoPausedDialogNotVisible", false)) {
                final MaterialDialog videoPausedDialog = DialogHelper.videoPausedDialog(this);
                videoPausedDialog.getBuilder().onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getPreferences(MODE_PRIVATE).edit().putBoolean("videoPausedDialogNotVisible", dialog.isPromptCheckBoxChecked()).apply();
                        initializePlayer();
                    }
                });
            } else
                initializePlayer();
        }
        else {
            youTubePlayerFragment.getView().setVisibility(View.GONE);
            context = this;
            roomClosedListener =new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists())
                        DialogHelper.roomClosed(context).getBuilder().onAny(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
            databaseReference.addValueEventListener(roomClosedListener);
        }


    }
    public void initializePlayer(){
        youTubePlayerFragment.initialize(getResources().getString(R.string.API_KEY), new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
                if(!b) {
                    TubePlayer = youTubePlayer;
                    TubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
                    TubePlayer.setShowFullscreenButton(false);
                    nextVideo();

                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.i("RoomAdminActivity","Player not visible");
        unregisterReceiver(connectionReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        connectionReceiver=new InternetConnectionReceiver();
        IntentFilter connectionReceiverIntent=new IntentFilter();
        connectionReceiverIntent.addAction(CONNECTIVITY_SERVICE);
        connectionReceiverIntent.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectionReceiver, connectionReceiverIntent);

    }

    @Override
    protected void onDestroy() {
            super.onDestroy();
            if(initialized) {
                databaseReference.removeValue();
                iconReference.delete();
                TubePlayer.release();
            }
            else
                databaseReference.removeEventListener(roomClosedListener);

    }

    public Video getCurrentVideo(){
        return this.currentVideo;
    }

    public void nextVideo() {
        if (AdminVideoListFragment.allVideos.isEmpty()
                || AdminVideoListFragment.allVideos.get(AdminVideoListFragment.allVideos.size()-1).equals(currentVideo)) {
            AdminVideoListFragment.allVideos.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<Video>>() {
                @Override
                public void onChanged(ObservableList<Video> videos) {

                }

                @Override
                public void onItemRangeChanged(ObservableList<Video> videos, int i, int i1) {

                }

                @Override
                public void onItemRangeInserted(ObservableList<Video> videos, int i, int i1) {
                    currentVideo = videos.get(i);
                    databaseReference.child("currentVideo").setValue(currentVideo);
                    AdminVideoListFragment.allVideos.removeOnListChangedCallback(this);
                    TubePlayer.loadVideo(videos.get(i).getLink());
                }

                @Override
                public void onItemRangeMoved(ObservableList<Video> videos, int i, int i1, int i2) {

                }

                @Override
                public void onItemRangeRemoved(ObservableList<Video> videos, int i, int i1) {

                }
            });
        }
        else{
            currentVideo = AdminVideoListFragment.allVideos.get(AdminVideoListFragment.allVideos.indexOf(currentVideo)+1);
            databaseReference.child("currentVideo").setValue(currentVideo);
            TubePlayer.loadVideo(currentVideo.getLink());
        }
    }

}
