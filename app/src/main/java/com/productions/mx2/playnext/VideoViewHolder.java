package com.productions.mx2.playnext;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeThumbnailView;

public class VideoViewHolder extends RecyclerView.ViewHolder {
    public TextView title, length, user;
    public YouTubeThumbnailView thumbnail;
    public View view;


    public VideoViewHolder(View view) {
        super(view);
        this.view=view;
        title=view.findViewById(R.id.video_title);
        length=view.findViewById(R.id.video_length);
        user=view.findViewById(R.id.added_by_user);
        thumbnail= view.findViewById(R.id.video_thumbnail);
    }

    public View getView(){
        return view;
    }
}
