package com.productions.mx2.playnext;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.firebase.database.DatabaseReference;


import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class DialogHelper {

    public static MaterialDialog addVideoDialog(final Context context, final DatabaseReference databaseReference, final String nickname){

        final MaterialDialog dialog=
                new MaterialDialog.Builder(context)
                        .title(R.string.add_video)
                        .customView(R.layout.add_video_dialog,false)
                        .positiveText(R.string.search)
                        .negativeText(R.string.done)
                        .neutralText(R.string.help)
                        .autoDismiss(false)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .widgetColorRes(R.color.colorPrimary)
                        .build();

        //noinspection ConstantConditions
        final EditText videoLinkEditText= dialog.getCustomView().findViewById(R.id.video_link);
        final RecyclerView searchedVideosRV = dialog.getCustomView().findViewById(R.id.search_results_recyclerview);
        searchedVideosRV.setHasFixedSize(true);
        searchedVideosRV.setLayoutManager(new LinearLayoutManager(context));

        class searchResults extends AsyncTask<Void, Void, Void>{
            String searchQuery;
            HttpTransport httpTransport;
            JsonFactory jsonFactory;
            YouTube youTube;
            SearchListResponse response;
            SearchVideosAdepter searchVideosAdepter;

            public searchResults(String query) {
                httpTransport= AndroidHttp.newCompatibleTransport();
                jsonFactory= JacksonFactory.getDefaultInstance();
                youTube=new YouTube.Builder(httpTransport,jsonFactory,null)
                        .setApplicationName(context.getResources().getString(R.string.app_name))
                        .build();
                searchQuery=query;
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    YouTube.Search.List searchListByKeywordRequest =  youTube.search().list("snippet");
                    searchListByKeywordRequest.setMaxResults(10L);
                    searchListByKeywordRequest.setQ(searchQuery);
                    searchListByKeywordRequest.setType("video");
                    searchListByKeywordRequest.setKey(context.getResources().getString(R.string.API_KEY));
                    response = searchListByKeywordRequest.execute();
                    return null;
                }
                catch (Throwable e){
                    e.printStackTrace();
                    Log.i("searchResults","Something went wrong while searching videos :/");
                }
                finally {
                    return null;
                }

            }


            @Override
            protected void onPostExecute(Void aVoid) {
                dialog.getCustomView().findViewById(R.id.video_search_progressBar).setVisibility(View.GONE);
                searchedVideosRV.setVisibility(View.VISIBLE);
                searchVideosAdepter=new SearchVideosAdepter(response.getItems(),context,databaseReference,nickname);
                searchedVideosRV.setAdapter(searchVideosAdepter);
            }

        }


        final TextView.OnEditorActionListener searchListener=new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(textView.getText().toString().length()==0) {
                    return true;
                }
                searchedVideosRV.setVisibility(View.GONE);
                dialog.getCustomView().findViewById(R.id.video_search_progressBar).setVisibility(View.VISIBLE);
                new searchResults(textView.getText().toString()).execute();
                return false;
            }
        };
        videoLinkEditText.setOnEditorActionListener(searchListener);
        dialog.getBuilder().onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                searchListener.onEditorAction(videoLinkEditText,EditorInfo.IME_NULL,null);
            }
        });
        dialog.show();
        return dialog;
    }



    public static MaterialDialog roomDialog(final Context context, boolean newRoom){

        MaterialDialog dialog=
                new MaterialDialog.Builder(context)
                        .title(newRoom ? R.string.create_room:R.string.join_room)
                        .customView(R.layout.create_room_dialog,true)
                        .positiveText(newRoom ? R.string.create:R.string.join)
                        .negativeText(R.string.cancel)
                        .neutralText(R.string.help)
                        .widgetColorRes(R.color.colorPrimary)
                        .build();
        final MDButton positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        //noinspection ConstantConditions
        final EditText nicknameET= dialog.getCustomView().findViewById(R.id.nickname);
        final EditText roomNameET = dialog.getCustomView().findViewById(R.id.roomName);
        roomNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                positiveAction.setEnabled(charSequence.toString().trim().length() > 0 && nicknameET.getText().toString().trim().length()>0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        nicknameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                positiveAction.setEnabled(charSequence.toString().trim().length() > 0 && roomNameET.getText().toString().trim().length()>0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        positiveAction.setEnabled(false);
        dialog.show();
        return dialog;
    }

    public static MaterialDialog videoPausedDialog(Context context){
        return new MaterialDialog.Builder(context)
                .iconRes(R.drawable.ic_warning_black_24dp)
                .limitIconToDefaultSize()
                .title(R.string.warning)
                .content(R.string.video_paused_dialog_content)
                .positiveText(R.string.got_it)
                .checkBoxPromptRes(R.string.dont_show_again, false, null)
                .show();
    }

    public static MaterialDialog saveVideoDialog(Context context, final String id, final String title, final String duration, final String uploader, final byte[] image){
        final VideosDatabaseHelper videosDatabaseHelper = new VideosDatabaseHelper(context);
        final AdminVideoListFragment videoListFragment= (AdminVideoListFragment) ((AppCompatActivity) context).getSupportFragmentManager().findFragmentById(R.id.video_list_container);
        final MaterialDialog dialog=new MaterialDialog.Builder(context)
                .iconRes(R.drawable.ic_file_download_grey_24dp)
                .limitIconToDefaultSize()
                .title(R.string.save_this_video)
                .content(R.string.save_this_video_content)
                .contentGravity(GravityEnum.CENTER)
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .show();

        final MaterialDialog.SingleButtonCallback positiveSaveVideo=new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull final MaterialDialog methodDialog, @NonNull final DialogAction which) {
                switch (videosDatabaseHelper.insertVideo(id, title, duration, uploader, image)) {
                    case 0:
                        videoListFragment.makeSnackbar(R.string.video_already_exists,Snackbar.LENGTH_SHORT,Color.YELLOW).show();
                        return;
                    case -1:
                        Snackbar snackbar = videoListFragment.makeSnackbar(R.string.video_not_saved,Snackbar.LENGTH_SHORT,Color.RED);
                        snackbar.setAction(R.string.try_again, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.getActionButton(which).callOnClick();
                                return;
                            }
                        });
                        snackbar.show();
                        return;
                    case 1:
                        videoListFragment.makeSnackbar(R.string.video_saved_successfully,Snackbar.LENGTH_SHORT,Color.GREEN).show();
                        return;
                }
            }
        };
        dialog.getBuilder().onPositive(positiveSaveVideo);
        return dialog;
    }

    public static MaterialDialog addOfflineVideoDialog(Context context, DatabaseReference roomReference, String nickname) {
        final VideosDatabaseHelper databaseHelper=new VideosDatabaseHelper(context);
        List<HashMap> videosList=databaseHelper.getAllDataList();
        final OfflineVideosAdepter adepter=new OfflineVideosAdepter(context, videosList, roomReference, nickname);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
        MaterialDialog dialog =
                new MaterialDialog.Builder(context)
                        .title(R.string.add_video)
                        .positiveText(R.string.done)
                        .negativeText(R.string.cancel)
                        .neutralText(R.string.help)
                        .adapter(adepter,linearLayoutManager)
                        .autoDismiss(true)
                        .widgetColorRes(R.color.colorPrimary)
                        .build();
        RecyclerView videosRV=dialog.getRecyclerView();
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                databaseHelper.deleteData((String)((VideoViewHolder) viewHolder).getView().getTag());
                adepter.updateData(databaseHelper.getAllDataList());
            }
        }).attachToRecyclerView(videosRV);
        dialog.show();
        return dialog;
    }

    public static MaterialDialog roomExistsDialog(Context context,boolean exists) {
        return new MaterialDialog.Builder(context)
                .title(exists ? R.string.room_taken : R.string.room_doesnt_exists)
                .iconRes(R.drawable.ic_error_black_24dp)
                .content(exists ? R.string.room_taken_content : R.string.room_doesnt_exists_content)
                .positiveText(R.string.try_again)
                .negativeText(R.string.cancel)
                .autoDismiss(true)
                .widgetColorRes(R.color.colorPrimary)
                .show();
    }

    public static MaterialDialog roomClosed(Context context) {
        MaterialDialog dialog =
                new MaterialDialog.Builder(context)
                        .title(R.string.room_closed)
                        .iconRes(R.drawable.ic_error_black_24dp)
                        .content(R.string.room_closed_content)
                        .positiveText(R.string.confirm)
                        .cancelable(false)
                        .canceledOnTouchOutside(false)
                        .widgetColorRes(R.color.colorPrimary)
                        .build();
        dialog.show();
        return dialog;
    }

    public static String parseDuration(String length){
        char[] d=length.toCharArray();
        String s="00";
        String m="00";
        String h="";
        if(d[0] != 'P') {
            Log.i("parseDuration", "cant parse. not ISO 8601");
            return null;
        }
        for (int i=1; i<d.length; i++)
            switch (d[i]) {
                case 'S':s="";
                    for (int j=i-1; d[j]!='T' && d[j]!='M';j--)
                        s+=d[j];
                    s=new StringBuilder(s).reverse().toString();
                    break;
                case 'M':m="";
                    for (int j=i-1; d[j]!='T' && d[j]!='H';j--)
                        m+=d[j];
                    m=new StringBuilder(m).reverse().toString();
                    m+=":";
                    break;
                case 'H':for (int j=i-1; d[j]!='T' && d[j]!='H';j--)
                    h+=d[j];
                    h=new StringBuilder(h).reverse().toString();
                    h+=":";
                    break;
                default: break;
            }
        if (s.length()==1)
            s="0" + s;
        if (m.length()==1)
            m="0" +m;
        if (h.length()>0)
            return h + m + s;
        return m+s;
    }

    public static String parseDate (DateTime date, Context context){
        String ago=context.getResources().getString(R.string.ago);
        Calendar currentCalendar= Calendar.getInstance();
        Calendar uploadCalender= Calendar.getInstance();
        uploadCalender.setTimeInMillis(date.getValue());
        if (currentCalendar.get(Calendar.YEAR)==uploadCalender.get(Calendar.YEAR)) {
            if (currentCalendar.get(Calendar.MONTH) == uploadCalender.get(Calendar.MONTH)) {
                if (currentCalendar.get(Calendar.DAY_OF_MONTH) == uploadCalender.get(Calendar.DAY_OF_MONTH)) {
                    if (currentCalendar.get(Calendar.HOUR_OF_DAY) == uploadCalender.get(Calendar.HOUR_OF_DAY))
                        return context.getResources().getString(R.string.right_now) + "!";
                    else
                        return  (currentCalendar.get(Calendar.HOUR_OF_DAY) - uploadCalender.get(Calendar.HOUR_OF_DAY)) + " " + context.getResources().getString(R.string.hours)+ " " + ago;
                } else
                    return  (currentCalendar.get(Calendar.DAY_OF_MONTH) - uploadCalender.get(Calendar.DAY_OF_MONTH)) + " " + context.getResources().getString(R.string.days)+ " " + ago;
            } else
                return  (currentCalendar.get(Calendar.MONTH) - uploadCalender.get(Calendar.MONTH)) + " " + context.getResources().getString(R.string.months)+ " " + ago;
        }
        else
            return (currentCalendar.get(Calendar.YEAR)-uploadCalender.get(Calendar.YEAR))+ " " + context.getResources().getString(R.string.years)+ " " + ago;

    }



}
