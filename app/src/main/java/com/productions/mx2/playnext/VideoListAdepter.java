package com.productions.mx2.playnext;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.VideoListResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by maorm on 16/02/2018. Well, after taking some time off i decided to come back and
 * finish this project. It won't be what I imagined but that's only due to the deadline.
 * Eventually it will be what we all planned it to be.
 *
 * I delayed the advanced recyclerview and decided to do it with only one recycler view.
 * This is his adapter.
 */

public class VideoListAdepter extends RecyclerView.Adapter<VideoViewHolder> {


    List<Video> videoList;
    protected Context context;

    public VideoListAdepter (Context context, List<Video> videos){
        videoList=videos;
        this.context=context;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_video, parent, false);
        return new VideoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoViewHolder holder, int position) {

        final Video video=videoList.get(position);
        //TODO: find a way to store all the items(temp solution)
        final View.OnLongClickListener saveVideoListener=new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                DialogHelper.saveVideoDialog(context,video.getLink(),
                        holder.title.getText().toString(),
                        holder.length.getText().toString(),
                        "",
                        getBytes(((BitmapDrawable)holder.thumbnail.getDrawable()).getBitmap()));
                return false;
            }
        };

        class YoutubeHelper extends AsyncTask<Void, Void, Void> {

            HttpTransport httpTransport;
            JsonFactory jsonFactory;
            YouTube youTube;
            VideoListResponse response;
            Context context;
            YouTubeThumbnailLoader ytl;

            public YoutubeHelper(Context baseContext, YouTubeThumbnailLoader youTubeThumbnailLoader){
                context=baseContext;
                httpTransport= AndroidHttp.newCompatibleTransport();
                jsonFactory= JacksonFactory.getDefaultInstance();
                youTube=new YouTube.Builder(httpTransport,jsonFactory,null)
                        .setApplicationName(context.getResources().getString(R.string.app_name))
                        .build();
                ytl = youTubeThumbnailLoader;
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {

                    YouTube.Videos.List videoNameByIdRequest = youTube.videos().list("snippet,contentDetails");
                    videoNameByIdRequest.setId(video.getLink());
                    videoNameByIdRequest.setKey(context.getResources().getString(R.string.API_KEY));
                    response = videoNameByIdRequest.execute();
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.i("YoutubeHelper","Unable to connect to Youtube data api.");
                    return null;
                }
                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {
                final com.google.api.services.youtube.model.Video responseVid = response.getItems().get(0);
                ytl.setVideo(video.getLink());
                ytl.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                        ytl.release();
                        holder.title.setText(responseVid.getSnippet().getTitle());
                        holder.length.setText(DialogHelper.parseDuration(responseVid.getContentDetails().getDuration()));
                        holder.user.setText(context.getResources().getString(R.string.added_by)+ (video.getUser()));
                        AppCompatActivity baseActivity=(RoomAdminActivity) context;
                        AdminVideoListFragment baseFragment =(AdminVideoListFragment) baseActivity.getSupportFragmentManager().findFragmentById(R.id.video_list_container);
                        if (baseFragment.getCurrentVideo()==video) {
                            baseFragment.currentVideoPlayedBG();
                        }
                        holder.getView().setOnLongClickListener(saveVideoListener);
                    }

                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                        Log.i("VideoListAdepter", video.getLink() + "Thumbnail load failed");
                    }
                });

            }
        }

        holder.thumbnail.initialize(context.getResources().getString(R.string.API_KEY), new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView,final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                new YoutubeHelper(context, youTubeThumbnailLoader).execute();

            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                Log.i("VideoListAdepter","Failed to initialize thumbnail.");
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public byte[] getBytes(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toByteArray();
    }

}
