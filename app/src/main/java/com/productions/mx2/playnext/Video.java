package com.productions.mx2.playnext;

/**
 * Created by maorm on 03/03/2018.
 * Simple video class. Might change in the future.
 */

public class Video {

    private String link;
    private String user;

    public Video(){
        link="";
        user="";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != Video.class)
            return false;
        Video v=(Video) obj;
        return v.getLink().equals(link);
    }

    public Video(String l, String u){
        link=l;
        user=u;
    }

    public String getUser(){ return user;}
    public String getLink(){ return link;}
}
