package com.productions.mx2.playnext;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class MenuActivity extends AppCompatActivity {
    String roomNameString, nicknameString;

    static final int REQUEST_IMAGE_CAPTURE = 71;

    FirebaseStorage storage;
    StorageReference storageReference;

    MaterialDialog joiningProgressDialog;
    MaterialDialog creatingProgressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar tb= findViewById(R.id.ToolBar);
        setSupportActionBar(tb);

        storage = FirebaseStorage.getInstance();
        storageReference=storage.getReference();

        joiningProgressDialog = new MaterialDialog.Builder(this)
                .title(R.string.joining_room)
                .content(R.string.have_fun)
                .progress(true, 0)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .build();
        creatingProgressDialog = new MaterialDialog.Builder(this)
                .title(R.string.creating_room)
                .content(R.string.have_fun)
                .progress(true, 0)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .build();
    }


    public void createRoomDialog(View view) {
        roomNameString="";
        nicknameString="";
        final Context context= this;
        MaterialDialog dialog =DialogHelper.roomDialog(this, true);
        dialog.getBuilder().onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                if(isConnectedToInternet()) {
                    roomNameString = ((EditText)dialog.getView().findViewById(R.id.roomName)).getText().toString();
                    nicknameString = ((EditText)dialog.getView().findViewById(R.id.nickname)).getText().toString();
                    creatingProgressDialog.show();
                    roomExists().addOnSuccessListener(new OnSuccessListener<Boolean>() {
                        @Override
                        public void onSuccess(Boolean exists) {
                            if (exists){
                                creatingProgressDialog.dismiss();
                                DialogHelper.roomExistsDialog(context, true).getBuilder().onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        createRoomDialog(null);
                                    }
                                });
                            }
                            else
                                createRoom();

                        }
                    });
                }
                else
                    Toast.makeText(context,R.string.no_internet_connection,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createRoom() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        else {
            Bitmap icon= BitmapFactory.decodeResource(getResources(),R.drawable.playnext);
            Intent logo=new Intent();
            logo.putExtra("data",icon);
            onActivityResult(REQUEST_IMAGE_CAPTURE,RESULT_OK,logo);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            creatingProgressDialog.show();

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            final StorageReference ref=storageReference.child("rooms/"+roomNameString+".jpg");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] dataArray = baos.toByteArray();
            FirebaseAuth.getInstance().signInAnonymously().addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    final UploadTask uploadTask= ref.putBytes(dataArray);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            Log.i("MenuActivity", "unsuccessfully uploaded image");
                            creatingProgressDialog.dismiss();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Log.i("MenuActivity", "successfully uploaded" + taskSnapshot.getMetadata().getSizeBytes() + "Bytes");
                            Intent i=new Intent(getBaseContext() , RoomAdminActivity.class);
                            i.putExtra("roomname", roomNameString);
                            i.putExtra("nickname", nicknameString);
                            i.putExtra("initialized",true);
                            creatingProgressDialog.dismiss();
                            startActivity(i);
                        }
                    });
                }
            });

        }
    }

    public void joinRoomDialog(View view) {
        roomNameString = "";
        nicknameString = "";
        final Context context = this;
        MaterialDialog dialog = DialogHelper.roomDialog(this, false);
        dialog.getBuilder().onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                roomNameString = ((EditText) dialog.getView().findViewById(R.id.roomName)).getText().toString();
                nicknameString = ((EditText) dialog.getView().findViewById(R.id.nickname)).getText().toString();
                if(isConnectedToInternet())
                    joinRoom();
                else
                    Toast.makeText(context,R.string.no_internet_connection,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void joinRoom() {
        final Context context=this;
        joiningProgressDialog.show();
        roomExists().addOnSuccessListener(new OnSuccessListener<Boolean>() {
            @Override
            public void onSuccess(Boolean exists) {
                if (!exists){
                    joiningProgressDialog.dismiss();
                    DialogHelper.roomExistsDialog(context, false).getBuilder().onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            joinRoomDialog(null);
                        }
                    });
                }
                else {
                    Intent i=new Intent(getBaseContext() , RoomAdminActivity.class);
                    i.putExtra("roomname", roomNameString);
                    i.putExtra("nickname", nicknameString);
                    i.putExtra("initialized",false);
                    joiningProgressDialog.dismiss();
                    startActivity(i);
                }
            }
        });
    }

    public boolean isConnectedToInternet(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return  activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }



    public Task<Boolean> roomExists(){
        final TaskCompletionSource tcs=new TaskCompletionSource();
        final DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference();
        DatabaseReference roomReference= databaseReference.child(roomNameString);
        roomReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    tcs.setResult(true);
                else
                    tcs.setResult(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                tcs.setException(databaseError.toException());
            }
        });
        return tcs.getTask();
    }

    public void InformationScreen(View view) {
        Intent i=new Intent(this, SettingsActivity.class);
        startActivity(i);
    }
}
