package com.productions.mx2.playnext;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.api.services.youtube.model.SearchResult;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

public class SearchVideosAdepter extends RecyclerView.Adapter<VideoViewHolder> {

    Context context;
    List<SearchResult> searchResults;
    DatabaseReference roomReference;
    String nickname;
    final float titleSize = 14f;
    final float infoSize = 12f;
    LinearLayout.LayoutParams params;

    public SearchVideosAdepter(List<SearchResult> searchResults, Context context, DatabaseReference roomReference,String nickname) {
        this.searchResults=searchResults;
        this.context=context;
        this.roomReference=roomReference;
        this.nickname=nickname;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_video, parent, false);
        return new VideoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoViewHolder holder,final int position) {
        final View.OnClickListener holderClicked=new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new VideoUploader().uploadVideo(roomReference, searchResults.get(position).getId().getVideoId(), nickname)
                        .addOnSuccessListener(new OnSuccessListener<Integer>() {
                            @Override
                            public void onSuccess(Integer integer) {
                                switch (integer) {
                                    case VideoUploader.UPLOAD_SUCCESSFULLY:
                                        Toast.makeText(context, R.string.video_uploaded_successfully, Toast.LENGTH_SHORT).show();
                                        break;
                                    case VideoUploader.UPLOAD_FAILED:
                                        Toast.makeText(context, R.string.video_upload_failed, Toast.LENGTH_SHORT).show();
                                        break;
                                    case VideoUploader.VIDEO_EXISTS:
                                        Toast.makeText(context, R.string.video_already_exists, Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }
                        });
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, R.string.video_upload_failed, Toast.LENGTH_SHORT).show();
                }
            }
        };

        holder.title.setTextSize(TypedValue.COMPLEX_UNIT_SP,titleSize);
        params = (LinearLayout.LayoutParams)holder.title.getLayoutParams();
        params.setMargins(0,0,0,0);
        holder.title.setLayoutParams(params);
        holder.length.setTextSize(TypedValue.COMPLEX_UNIT_SP,infoSize);
        holder.user.setTextSize(TypedValue.COMPLEX_UNIT_SP,infoSize);
        holder.thumbnail.initialize(context.getResources().getString(R.string.API_KEY),
                new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, final YouTubeThumbnailLoader youTubeThumbnailLoader) {

                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                                youTubeThumbnailLoader.release();
                                holder.title.setText(searchResults.get(position).getSnippet().getTitle());
                                holder.length.setText(DialogHelper.parseDate(searchResults.get(position).getSnippet().getPublishedAt(), context));
                                holder.user.setText(searchResults.get(position).getSnippet().getChannelTitle());
                                holder.getView().setOnClickListener(holderClicked);
                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                            }

                        });
                        youTubeThumbnailLoader.setVideo(searchResults.get(position).getId().getVideoId());
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                        Log.i("VideoListAdepter","Failed to initialize thumbnail.");
                    }
                });

    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
