package com.productions.mx2.playnext;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class VideoUploader {
    final static int UPLOAD_SUCCESSFULLY = 1;
    final static int UPLOAD_FAILED = -1;
    final static int VIDEO_EXISTS = 0;
    final static int VIDEO_DONT_EXISTS = 2;


    public Task<Integer> uploadVideo(final DatabaseReference roomReference, final String link, final String user){
        final TaskCompletionSource tcs=new TaskCompletionSource();
        videoExists(roomReference, link, user)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        tcs.setResult(UPLOAD_FAILED);
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Integer>() {
                    @Override
                    public void onSuccess(Integer integer) {
                        if(integer==VIDEO_DONT_EXISTS) {
                            roomReference.push().setValue(new Video(link, user));
                            tcs.setResult(UPLOAD_SUCCESSFULLY);
                            return;
                        }
                        tcs.setResult(integer);
                    }
                });
        return tcs.getTask();
    }

    private Task<Integer> videoExists(final DatabaseReference roomReference, final String link, final String user) {
        final TaskCompletionSource<Integer> tcs = new TaskCompletionSource();

        roomReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot videoSnapshot:dataSnapshot.getChildren()) {
                    if (videoSnapshot.child("link").getValue()!=null && videoSnapshot.child("link").getValue().equals(link)) {
                        tcs.setResult(VIDEO_EXISTS);
                        return;
                    }
                }
                tcs.setResult(VIDEO_DONT_EXISTS);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                tcs.setResult(UPLOAD_FAILED);
            }
        });

        return tcs.getTask();
    }



}
